package Laba2.Utils;

import Laba2.SMO_types.RAND;
/**
 * Created by Bohdan on 08.10.2014.
 */
public class Settings {

    private double ne,lambda,actual,delta;

    private int n,bound;

    private static Settings instance = new Settings();

    private Settings(){
        //this.ne = 10;
       // this.lambda = 10;
        this.n = 100;
        this.lambda = 0.7;
        this.actual = Integer.MAX_VALUE;
       // this.delta = 0.001;
        //this.bound = 3;
    }

    public int getBound() {
        return bound;
    }

    public void setBound(int bound) {
        this.bound = bound;
    }

    public double getDelta(){
        return delta;
    }

    public static Settings getInstance(){
        return instance;
    }

    public double getNe(){
        return ne;
    }

    public double getLambda(){
        return lambda;
    }

    public int getN(){
        return n;
    }

    public double getActual(){
        return actual;
    }

    public static void main(String[] args) {
         new Statistics().output(new RAND().getSolved());
    }
}
