package Laba2.Utils;

import Laba2.Task;
import Laba2.SMO_types.RAND.*;

import java.util.*;

import static java.lang.Math.*;
/**
 * Created by Bohdan on 20.10.2014.
 */
public class Statistics {

    private LinkedList<Task> tasks = new LinkedList<Task>();
    private double timeInProc;
    private double timeInQueue;
    private double allworkTime;
    public static double prosoty;

    public void output(LinkedList<Task> tasks) {
        this.tasks = tasks;
        double x2 = duspersiya();
        for (Task t:tasks)
            allworkTime+=t.getSolveTime();
        System.out.println("Процент простою: "+Math.ceil((prosoty/allworkTime)*100)+"%");
        System.out.println("Оброблено задач: "+tasks.size());
        System.out.println(
                "Середній час обробки: " + timeInProc/tasks.size()+"" +
                "\nСередній час в черзі: " + timeInQueue/tasks.size()+
                "\nСЕРЕДНІЙ ЧАС В СИСТЕМІ: "+(timeInQueue+timeInProc)/tasks.size());
       /* for (int i = 0; i < tasks.size(); i++) {
            System.out.println(tasks.get(i));
        }*/
        makeIntervals();
    }

    private double averageTimeInSystem(){
        timeInProc = 0;
        timeInQueue = 0;
        for (Task t:tasks){
            timeInQueue += t.getTimeInQueue();
            timeInProc += t.getSolveTime();
        }
        return (timeInQueue+timeInProc)/tasks.size();
    }

    private double duspersiya(){
        double averageTime = averageTimeInSystem();
        double s = 0;
        for(Task t:tasks){
            allworkTime+=t.getSolveTime();
            s+=pow(averageTime - (t.getSolveTime() + t.getTimeInQueue()),2);
        }
        return s/tasks.size() ;
    }

    private void makeIntervals(){
        HashMap<Pair,Increment> map = new HashMap<Pair, Increment>();
        for (int i = 0; i < 1000; i+=100) {
            Increment in = new Increment();
            in.inc();
            map.put(new Pair(i,i+100),in);
        }
        for (Task t:tasks) {
            for (Pair p:map.keySet()){
                if (p.getLeft() <= t.getTimeInQueue() &
                        p.getRight() >= t.getTimeInQueue()){
                            map.get(p).inc();
                            break;
                }
            }
        }
        ArrayList<Pair> pairs = new ArrayList<Pair>();
        for (Pair p: map.keySet()){
            pairs.add(p);
        }
        Collections.sort(pairs);
        String ss[] = new String[pairs.size()];
        for (int i = 0; i < pairs.size(); i++) {
            for (Pair p : map.keySet()) {
                if (pairs.get(i).equals(p)){
                    ss[i] = p + " = " +  (i==0 ? map.get(p).getValue()/3:map.get(p));
                    break;
                }
            }
        }
        for(String s:ss)
            System.out.println(s.split(" = ")[1]);
    }

    public class Increment{
        private int k;

        public Increment(){
        }
        public void inc(){
            k++;
        }

        public int getValue(){
            return k;
        }

        public String toString(){
            return String.valueOf(k);
        }
    }

    public class Pair implements Comparable<Pair>{

        private int left;
        private int right;

        public int getLeft() {
            return left;
        }

        public int getRight() {
            return right;
        }

        @Override
        public boolean equals(Object p){
            Pair pair = (Pair)p;
            if (p==null) return false;
            if (!(p instanceof Pair))return false;
            if(this.getLeft() == pair.getLeft())
                return true;
            return false;
        }

        public Pair(int l,int r){
            this.left = l;
            this.right = r;
        }

        public String toString(){
            return left+" "+ right;
        }

        @Override
        public int compareTo(Pair o) {
            return this.getLeft() > o.getLeft() ? 1 :-1;
        }
    }
}
