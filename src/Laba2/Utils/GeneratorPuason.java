package Laba2.Utils;

import Laba2.Task;

import java.util.LinkedList;
import java.util.Random;

/**
 * Created by Bohdan on 13.03.2015.
 */
public class GeneratorPuason {

    public static GeneratorPuason gen = new GeneratorPuason();

    private LinkedList<Task> tasks;

    private Random random;

    private GeneratorPuason(){
        tasks = new LinkedList<Task>();
        random = new Random();
        puasson();
    }

    private double getSolveTime(){
        int bound = 20;
        int t = random.nextInt(bound);
        return t != 0 ? t : 1;
    }

    public static GeneratorPuason getInstance(){
        return gen;
    }

    public LinkedList<Task> getTasks(){
        return tasks;
    }

    private void puasson(){
        double l = Settings.getInstance().getLambda();
        double time = 0;
        int n = 0;
        do {
            time+=-1/l*Math.log(random.nextDouble());
            n++;
            tasks.add(new Task(Math.ceil(time),getSolveTime(),0));
        }while (n < Settings.getInstance().getN());
    }

    public static void main(String[] args) {
       GeneratorPuason.getInstance();
    }
}
