package Laba2.Utils;

import Laba2.Task;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.Random;

/**
 * Created by Bohdan on 08.10.2014.
 */
public class Generator {

    private double lamda;

    private double a;

    private Random r = new Random();

    private double right;

    private double delta;

    private double len;

    private LinkedList<Task> tasks;

    private static Generator instance = new Generator();

    public static Generator getInstance(){
        return instance;
    }

    private Generator(){
        this.lamda = Settings.getInstance().getLambda();
        double ne = Settings.getInstance().getNe();
        double center = (double)1/ne;
        a = ne - 0.3333333333333333;
        double buf = 0;
        if(a > center){
            right = a;
            buf = a;
            a = 2*center - buf;
            delta = right - center;
        }else{
            delta = (center - a);
            right = (a + 2*delta);
        }
        len = right - a;

        System.out.println("left = " + a +
                "\ncentr = " + center +
                "\nright = " + right +
                "\ndelta = " + delta
                //"\nlength = " + len
                );
        tasks = new LinkedList<Task>();
        Random r = new Random();
        int bound = Settings.getInstance().getBound();
        tasks.add(new Task(round(this.getEnterTime()),round(this.getSolveTime()),r.nextInt(bound)));
        for (int i = 1; i < Settings.getInstance().getN(); i++) {
                Task t = new Task(tasks.get(i - 1).getEnterTime() + this.getEnterTime(), this.getSolveTime(),r.nextInt(bound));
                tasks.add(t);
        }
    }

    public LinkedList<Task> getTasks(){
        return  tasks;
    }

    public double getEnterTime(){
        return -1/lamda*Math.log(r.nextDouble());
    }

    public double getSolveTime() {
          double st = 0;
          do {
              st = len *(r.nextDouble()) + delta;
           }while (st < a || st > right);
       return st;
    }
    private double round(double d){
        return new BigDecimal(d).setScale(2,RoundingMode.HALF_UP).doubleValue();
    }

    private void show(LinkedList<Task> tasks){
        for (Task t:tasks)
            System.out.println(t.getEnterTime()+" "+t.getSolveTime());
    }
}


