package Laba2.Utils;

import Laba2.Task;

import java.util.Comparator;

public class CompareTasksByPriority implements Comparator<Task> {
    @Override
    public int compare(Task o1, Task o2) {
        return o1.getPriority() == o2.getPriority()? 0 : o1.getPriority() < o2.getPriority()? 1 : -1;
    }
}