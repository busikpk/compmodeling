package Laba2;

import Laba2.Utils.Settings;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by Bohdan on 18.10.2014.
 */
public class Task {

    public static final double ETALON_TIME = Settings.getInstance().getActual();

    private double solveT;

    private double enterT;

    private int priority;

    private double timeInQueue;

    private double timeInSystem;

    private double reactionTime;

    private int enterIndexInQueue;

    public Task(double enterT,double solveT,int priority){
        this.enterT = enterT;
        this.solveT = solveT;
        this.priority = priority;
    }

    public Task(){
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void setEnterIndexInQueue(int index){
        this.enterIndexInQueue = index;
    }

    public int getEnterIndexInQueue(){
        return enterIndexInQueue;
    }

    public double getReactionTime(){
        return reactionTime;
    }

    public void setReactionTime(double t){
        this.reactionTime = t;
    }

    public double getTimeInProc() {
        return timeInSystem;
    }

    public void setTimeInProc(double timeInSystem) {
        this.timeInSystem = timeInSystem;
    }

    public void setSolveTime(double time){
        this.solveT = time;
    }

    public double getSolveTime(){
        return solveT;
    }

    public void setEnterTime(double time){
        this.enterT = time;
    }

    public double getEnterTime(){
        return enterT;
    }

    public double getTimeInQueue(){
        return timeInQueue;
    }

    public void setTimeInQueue(double time){
        this.timeInQueue = time;
    }

    private double round(double x){
        return new BigDecimal(x).setScale(1, RoundingMode.HALF_UP).doubleValue();
    }

    public String toString(){
        return "Task:{"+this.getEnterTime()+" " +this.getSolveTime()+" " +
                ""+this.getTimeInQueue()+"}";
    }
}
