package Laba2.SMO_types;

import Laba2.Utils.Generator;
import Laba2.Task;

import java.util.LinkedList;

/**
 * Created by Bohdan on 20.10.2014.
 */
public class FIFO extends SMO {

    public FIFO() {
        tasks = new LinkedList<Task>();
        for (Task t : Generator.getInstance().getTasks())
            tasks.add(new Task(t.getEnterTime(), t.getSolveTime(),t.getPriority()));
        queue = new LinkedList<Task>();
        solved = new LinkedList<Task>();
        solve();
    }
    private void solve() {
        System.out.println("\nFIFO");
        Task task = tasks.poll();
        procesStart = task.getEnterTime();
        while (true) {
            procesEnd = procesStart + task.getSolveTime();
            task.setTimeInProc(task.getSolveTime());
            double queTime;
            if(task.getEnterTime() >= procesStart ) {
                queTime = 0;
                procesStart = task.getEnterTime();
                procesEnd = procesStart + task.getSolveTime();
            }else
                queTime = (procesStart - task.getEnterTime());
            if ( queTime >= 0 ) {
                task.setReactionTime(queTime);
                task.setTimeInQueue(queTime);
                solved.add(task);
            }
            for (int i = 0; i < tasks.size(); i++) {
                if (tasks.get(i).getEnterTime() < procesEnd) {
                    queue.add(tasks.get(i));
                    tasks.remove(i);
                    i--;
                }
            }
            try {
                while (!isActual(queue.getFirst())) {
                    queue.removeFirst();
                }
                task = queue.poll();
                if (task == null) throw new Exception();
            } catch (Exception e) {
                if (!tasks.isEmpty()) task = tasks.poll();
                else
                    break;
            }
            procesStart = procesEnd;
        }
    }

    protected boolean isActual(Task t) {
        boolean buf = true;
        if (procesEnd - t.getEnterTime() > Task.ETALON_TIME) {
            //System.out.println(t.getEnterTime()+" "+(procesEnd - t.getEnterTime())+" "+t.getSolveTime());
            buf = false;
        }
        return buf;
    }
}