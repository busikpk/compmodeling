package Laba2.SMO_types;

import Laba2.Task;
import Laba2.Utils.Generator;
import java.util.LinkedList;

/**
 * Created by Bohdan on 25.10.2014.
 */
public class FB extends SMO {

    public FB(){
        tasks = new LinkedList<Task>();
        for(Task t: Generator.getInstance().getTasks())
            tasks.add(new Task(t.getEnterTime(),t.getSolveTime(),t.getPriority()));
        mainQueue = new LinkedList<Task>();
        secondQueue = new LinkedList<Task>();
        solved = new LinkedList<Task>();
        solve();
    }

    private void solve() {
        System.out.println("\nFB");
        Task task = tasks.poll();
        time = task.getEnterTime();
        solved.add(task);
        while (true){
            time += delta;
            fillMainQueue();

            while (!mainQueue.isEmpty()){
                if(isActual(mainQueue.getFirst())){
                   task = mainQueue.poll();
                   task.setTimeInProc(task.getTimeInProc() + delta);
                   task.setReactionTime(delta*task.getEnterIndexInQueue());
                   task.setTimeInQueue(task.getReactionTime());
                   if (task.getSolveTime() - task.getTimeInProc() <= 0){
                       solved.add(task);
                   }else{
                        secondQueue.add(task);
                   }
                }else{
                }
                time +=delta;
                fillMainQueue();
            }
            while(mainQueue.isEmpty()){
                if(secondQueue.isEmpty()) break;
                task = secondQueue.poll();
                if(isActual(task)){
                    task.setTimeInQueue(time - task.getEnterTime());
                    time += task.getSolveTime() - task.getTimeInProc();
                    solved.add(task);
                }
                fillMainQueue();
            }
            if (mainQueue.isEmpty() & secondQueue.isEmpty()){
                if (!tasks.isEmpty()){
                    task = tasks.poll();
                    time = task.getEnterTime();
                    mainQueue.add(task);
                }else break;
            }
        }          //mainQueue.removeFirst();

    }
    protected boolean isActual(Task t){
        if(time - t.getEnterTime() > Task.ETALON_TIME) return false;
        return true;
    }
    private void fillMainQueue(){
        for (int i = 0; i < tasks.size(); i++) {
            if(tasks.get(i).getEnterTime() < time + delta){
                tasks.get(i).setEnterIndexInQueue(mainQueue.size());
                mainQueue.add(tasks.get(i));
                tasks.remove(i);
                i--;
            }
        }
    }
}
