package Laba2.SMO_types;

import Laba2.Task;
import Laba2.Utils.Settings;

import java.util.LinkedList;

/**
 * Created by Bohdan on 25.10.2014.
 */
public abstract class SMO {

    protected LinkedList<Task> queue;

    protected LinkedList<Task> tasks;

    protected LinkedList<Task> mainQueue;

    protected LinkedList<Task> secondQueue;

    protected LinkedList<Task> solved;

    protected double delta = Settings.getInstance().getDelta();

    protected double time;

    protected double procesStart;

    protected double procesEnd;

    protected abstract boolean isActual(Task t);

    public LinkedList<Task> getSolved() {
        return solved;
    }

}
