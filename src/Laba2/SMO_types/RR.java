package Laba2.SMO_types;

import Laba2.Task;
import Laba2.Utils.CompareTasksByPriority;
import Laba2.Utils.Generator;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

/**
 * Created by Bohdan on 12.02.2015.
 */
public class RR extends SMO {

    public RR(){
        tasks = new LinkedList<Task>();
        for(Task t: Generator.getInstance().getTasks())
            tasks.add(new Task(t.getEnterTime(),t.getSolveTime(),t.getPriority()));
        mainQueue = new LinkedList<Task>();
        secondQueue = new LinkedList<Task>();
        solved = new LinkedList<Task>();
        solve();
    }

    public void solve(){
        System.out.println("RR");
        Task task = tasks.poll();
        time = task.getEnterTime();
        solved.add(task);
        while (true) {
            time += delta;
            fillMainQueue();
            //updateTasksInMainQueue();
            while (!mainQueue.isEmpty()){
                updateTasksInMainQueue();
                task = mainQueue.poll();
                task.setTimeInProc(task.getTimeInProc() + delta);
                task.setReactionTime(delta*task.getEnterIndexInQueue());
                if (task.getSolveTime() - task.getTimeInProc() <= 0){
                    solved.add(task);
                }else{
                    mainQueue.add(task);
                }
                time += delta;
                fillMainQueue();
                Collections.sort(mainQueue,new CompareTasksByPriority());
               // System.out.println(mainQueue);
            }
            while (mainQueue.isEmpty() & !tasks.isEmpty()){
                task = tasks.poll();
                task.setTimeInProc(task.getTimeInProc() + delta);
                task.setReactionTime(delta*task.getEnterIndexInQueue());
                if (task.getSolveTime() - task.getTimeInProc() <= 0){
                    solved.add(task);
                }else{
                    mainQueue.add(task);
                }
                time += delta;
                fillMainQueue();
                Collections.sort(mainQueue,new CompareTasksByPriority());
            }
            if (mainQueue.isEmpty() & tasks.isEmpty()) break;
        }

    }

    private void fillMainQueue(){
        for (int i = 0; i < tasks.size(); i++) {
            if(tasks.get(i).getEnterTime() < time + delta){
                tasks.get(i).setEnterIndexInQueue(mainQueue.size());
                mainQueue.add(tasks.get(i));
                tasks.remove(i);
                i--;
            }
        }
    }

    private void updateTasksInMainQueue(){
        for (int i = 0; i < mainQueue.size(); i++) {
            mainQueue.get(i).setTimeInQueue(
                    mainQueue.get(i).getTimeInQueue()+delta
            );
        }
    }
    @Override
    protected boolean isActual(Task t) {
        return false;
    }

}
