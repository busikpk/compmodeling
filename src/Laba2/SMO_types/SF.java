package Laba2.SMO_types;

import Laba2.Utils.Generator;
import Laba2.Task;

import java.util.LinkedList;
import java.util.Random;

/**
 * Created by Bohdan on 20.10.2014.
 */
public class SF extends SMO {

    private Random random = new Random();

    public SF(){
        tasks = new LinkedList<Task>();
        for (Task t: Generator.getInstance().getTasks()){
            tasks.add(new Task(t.getEnterTime(),t.getSolveTime(),t.getPriority()));
        }
        queue = new LinkedList<Task>();
        solved = new LinkedList<Task>();
        solve();
    }

    protected boolean isActual(Task t){
        boolean buf = true;
        if(procesEnd - t.getEnterTime() > Task.ETALON_TIME  ){
            //System.out.println(t.getEnterTime()+" "+(procesEnd - t.getEnterTime())+" "+t.getSolveTime());
            buf = false;
        }
        return buf;
    }

    private void solve(){
        System.out.println("\nSF");
        Task task = tasks.poll();
        procesStart = task.getEnterTime();
        while (true){
            procesEnd = procesStart + task.getSolveTime();
            task.setTimeInProc(task.getSolveTime());
            double queTime;
            if(task.getEnterTime() >= procesStart ) {
                queTime = 0;
                procesStart = task.getEnterTime();
                procesEnd = procesStart + task.getSolveTime();
            }
            else
                queTime = (procesStart - task.getEnterTime());
            if (queTime >= 0) {
                task.setReactionTime(queTime);
                task.setTimeInQueue(queTime);
                solved.add(task);
            }
            for (int i = 0; i < tasks.size() ; i++) {
                if(tasks.get(i).getEnterTime() < procesEnd){
                    queue.add(tasks.get(i));
                    tasks.remove(i);
                    i--;
                }
            }
            try {
                int index = getMinElementIndex();
                while(!isActual(queue.get(index))){
                    queue.remove(index);
                    index = getMinElementIndex();
                }
                task = queue.get(index);
                queue.remove(index);
                if (task == null) throw new Exception();
            }catch (Exception e){
                if(!tasks.isEmpty()) task = tasks.poll();
                else
                    break;
            }
            procesStart = procesEnd;
        }
    }
    private int getMinElementIndex(){
        int index = 0;
        double min = queue.getFirst().getSolveTime();
        for (int i = 0; i < queue.size(); i++){
            if(min > queue.get(i).getSolveTime()) index = i;
        }
        return index;
    }
}
