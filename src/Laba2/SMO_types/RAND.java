package Laba2.SMO_types;

import Laba2.Utils.Generator;
import Laba2.Task;
import Laba2.Utils.GeneratorPuason;
import Laba2.Utils.Statistics;

import java.util.*;

/**
 * Created by Bohdan on 08.10.2014.
 */
public class RAND extends SMO{

    private Random random = new Random();

    public RAND(){
        tasks = new LinkedList<Task>();
        for (Task t: GeneratorPuason.getInstance().getTasks()){
            tasks.add(new Task(t.getEnterTime(),t.getSolveTime(),t.getPriority()));
            //System.out.println(t);
        }
        queue = new LinkedList<Task>();
        solved = new LinkedList<Task>();
        solve();
    }

    protected boolean isActual(Task t){
        boolean buf = true;
        if(procesEnd - t.getEnterTime() > Task.ETALON_TIME ){
            buf = false;
        }
        return buf;
    }
    private double prostoy;

    private void solve(){
        System.out.println("\nRAND");
        Task task = tasks.poll();
        procesStart = task.getEnterTime();
        while (true){
            procesEnd = procesStart + task.getSolveTime();
            task.setTimeInProc(task.getSolveTime());
            double queTime;
            if(task.getEnterTime() >= procesStart ) {
                queTime = 0;
                procesStart = task.getEnterTime();
                procesEnd = procesStart + task.getSolveTime();
            } else
                queTime = (procesStart - task.getEnterTime());
            if (queTime >= 0) {
                task.setReactionTime(queTime);
                task.setTimeInQueue(queTime);
                solved.add(task);
            }
            fillQueue();
            try {
                int index = getRandomIndex();
                while(!isActual(queue.get(index))){
                    queue.remove(index);
                    index = getRandomIndex();
                }
                task = queue.get(index);
                queue.remove(index);
                if(task == null) throw new Exception();
            }catch (Exception e){
                if(!tasks.isEmpty()) task = tasks.poll();
                else
                    break;
                prostoy += task.getEnterTime() - procesEnd;
            }
            procesStart = procesEnd;
        }
        Statistics.prosoty = prostoy;
        System.out.println("Простой просесора: "+prostoy);
    }
    private int getRandomIndex(){
           return random.nextInt(queue.size());
    }

    private void fillQueue(){
        for (int i = 0; i < tasks.size() ; i++) {
            if(tasks.get(i).getEnterTime() < procesEnd){
                queue.add(tasks.get(i));
                tasks.remove(i);
                i--;
            }
        }
    }
    public double getProstoy(){
        return prostoy;
    }
}
